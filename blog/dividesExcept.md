# dividesExcept
Code challenge

### Description
You are given an array of integers arr.
One element of this array is NOT divisible by the GCD of all the other elements in the array.
Can you find the GCD and the single element that is not divisible by it?

It is guaranteed that there is one and only one element that isn't divisible by the GCD

Example
```
For arr = [ 15, 12, 18, 24, 25, 30 ], the answer should be [ 3, 25 ]
=> The GCD of all the other elements apart from 25 is 3.
```

### Solution
```javascript 
let dividesExcept = a =>{ 
    for(let i = 0; i < a.length ;i++) { 
        let g = gcda(a.filter(val => val !== a[i] )) 
        if(a[i] % g !== 0) { 
            return [g, a[i]] 
        } 
    } 
}

// calculate the GCD of two numbers let gcd = (a, b) => b == 0 ? a : gcd(b, a%b)

// calculate the GCD of the whole array let gcda = a => a.reduce(gcd) 
```
