# How to Write Your Own Promisify Function from Scratch

In this article, you will learn how to write your own promisify function from scratch.

### Callback function
```js
const getSum = (num1, num2, callback) => {

  if (typeof num1 !== 'number' || typeof num2 !== 'number') {
    return callback(new TypeError(""Invalid type""), null);
  }

  return callback(null, num1 + num2);
}
```

### Solution
```js
function promisify(func) {
  return function(...args) {
    return new Promise((resolve, reject) => {
      func.call(this, ...args, (error, data) => {
        if (error) {
          reject(error)
        } else {
          resolve(data)
        }
      })
    })
  }
}
```

### Test
```js
const getSumPromise = promisify(getSum);

getSumPromise(22, 22)
  .then(res => { console.log(res) }) // 44
  .catch(e => { console.log(e) })
```
