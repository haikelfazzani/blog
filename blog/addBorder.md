# addBorder

## Description
Given a rectangular matrix of characters, add a border of asterisks(*) to it.

picture = [""abc"",
           ""ded""]
the output should be

![Add border Code Chanllenge](https://i.ibb.co/k53CkGR/addborder.png)

## Solution
```java
String[] addBorder(String[] picture) {
    String[] r = new String[picture.length + 2];
    String star = "";
    for(int i = 0 ; i < picture[0].length()+2 ;i++){
        star += "*";
    }
    r[0] = star;
    for(int i = 0 ; i < picture.length ;i++){
        r[i+1] = "*" + picture[i] + "*";
    }
    r[r.length-1] = star;
    return r;
}
```
