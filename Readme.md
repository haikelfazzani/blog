### Security tools
- [kali](https://www.kali.org/tools/)
- [ubuntu](https://packages.ubuntu.com/)
- [ptf](https://github.com/trustedsec/ptf)
- [blackbuntu](https://github.com/neoslab/blackbuntu/blob/main/TOOLS.md)